/**
 * Created by htmlmak on 06.02.2018.
 */

// Polyfill from https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/String/startsWith

if (!String.prototype.startsWith) {
    Object.defineProperty(String.prototype, 'startsWith', {
        enumerable: false,
        configurable: false,
        writable: false,
        value: function (searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        }
    });
}

$(document).ready(function () {
    // Scroll to block function
    $(function () {
        window.scrollToBlock = function (target) {
            if (!target.jquery) {
                target = $(target);
            }
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 120
                }, 1000);
                return false;
            }
        };
    });

    // Input Mask
    (function ($) {
        $('#phone').inputmask("phone");
    })(jQuery);

    // Select type
    (function ($) {
        var dinamyc_field = $('.dynamic-field');

        $('#calc_type').on('change', function (e) {
            var type = Number(e.target.value);

            dinamyc_field.fadeOut().filter(function () {
                return $(this).data("type").indexOf(type) >= 0;
            }).stop().fadeIn(250);
            if (e.target.value !== 5) {
                $('.sub-field').fadeOut();
            }
        });

        var getInputWithMaxSize = function (queryset) {
            var top = {
                value: 0,
                element: null
            };
            queryset.each(function (index, item) {
                if (Number(item.value) > top.value) {
                    top.value = Number(item.value);
                    top.element = $(item);
                }
            });

            return top.element
        };

        var fixedMaxValue = function (queryset, offset) {
            var fix_total = offset;

            queryset.each(function (index, item) {
                fix_total += Number(item.value);
            });

            if (fix_total > 100) {
                var temp_element = getInputWithMaxSize(queryset);

                temp_element.data("ionRangeSlider").update({
                    from: Number(temp_element.val()) - 1
                });

                fixedMaxValue(queryset, offset)
            }
        };

        $('.range-slider').ionRangeSlider({
            min: 0,
            max: 100,
            from: 25,
            grid: false

        });
        /*
        Функция, которая дает выбрать максимум 100% на всех ползунках

        *             onFinish: function (object) {
                        var section = null;
                        var total = 0;
                        if (object.input.hasClass('current')) {
                            section = $('.range-slider.current');
                        } else {
                            section = $('.range-slider.upper');
                        }

                        section.each(function (index, item) {
                            total += Number(item.value);
                        });

                        if (total > 100) {
                            fixedMaxValue(section.not(object.input), Number(object.input.val()));
                        }
                    }*/
    })(jQuery);

    // sub Select type
    (function ($) {
        var dinamyc_field = $('.sub-field');

        $('#sub_type').on('change', function (e) {
            var type = Number(e.target.value);

            dinamyc_field.fadeOut().filter(function () {
                return $(this).data("type").indexOf(type) >= 0;
            }).stop().fadeIn(250);

        });

    })(jQuery);

    // Calc
    (function ($) {
        var chart_data = [];
        var ID = 0;

        window.Calc = function () {

            function createTable(tableData) {
                var table = document.createElement('table');
                table.classList.add("table");
                //table.classList = 'table';
                var tableBody = document.createElement('tbody');

                tableData.forEach(function (rowData) {
                    var row = document.createElement('tr');

                    rowData.forEach(function (cellData) {
                        var cell = document.createElement('td');
                        cell.appendChild(document.createTextNode(cellData));
                        row.appendChild(cell);
                    });

                    tableBody.appendChild(row);
                });

                table.appendChild(tableBody);

                $('#table').html(table)
            }

            function addRadioOption(object, index) {
                var template = '' +
                    '<div class="radio-inline">' +
                    '   <label>' +
                    '       <input name="plan-id" type="radio" value="' + object.id + '">' +
                    '       <span>план ' + index + '</span>' +
                    '</label>' +
                    '   <button class="close"></button>\n' +
                    '</div>';

                return $($.parseHTML(template));
            }


            return {
                init: function () {
                    $('body')
                        .on('click', '.radio-inline .close', function () {
                            Calc.removePlan($(this).closest('.radio-inline').find('input').val());
                            $("#send").addClass('disabled');
                        }).on('change', '.radio-inline input', function () {
                        $("#send").removeClass('disabled');
                    });

                    $('#repeat-calc').on('click', function () {
                        if ($(this).hasClass('disabled')) {
                            return alert('Одновременно можно рассчитать только 4 плана.');
                        }

                        scrollToBlock('#request-form');
                    });
                },

                addPlan: function (object) {
                    object.id = ID;
                    chart_data.push(object);

                    Calc.draw();
                    ID++;
                },

                removePlan: function (id) {
                    chart_data.forEach(function (element, index) {
                        if (element.id == id) {
                            return chart_data.splice(index, 1);
                        }
                    });

                    Calc.draw();
                },

                getPlan: function (id) {
                    var result = {};
                    chart_data.forEach(function (element, index) {
                        if (element.id == id) {
                            result = element
                        }
                    });

                    return result;
                },
                draw: function () {
                    var labels = [];
                    var series = [];
                    var table = [['Показатели'], ['Ваш план'], ['Возможные продажи'], ['% выполнения плана'], ['Оклад'], ['Надбавка'], ['Премия'], ['Годовой бонус'], ['Итого']];
                    var radioOption = $();

                    chart_data.forEach(function (element, index) {
                        labels.push('Расчёт ' + (index + 1));

                        table[0].push('Расчёт ' + (index + 1));

                        radioOption = radioOption.add(addRadioOption(element, index + 1));
                    });

                    $('#plan-select-wrap').html(radioOption);

                    chart_data.forEach(function (element) {

                        element.chart.forEach(function (chart, index) {

                            if (typeof series[index] != "object") {
                                series.push([])
                            }

                            var i = 0;
                            var sum = 0;

                            if (index > 0) {
                                sum = element.chart[index - 1]
                            }

                            series[index].push(chart - sum);
                        });

                        element.table.forEach(function (item, index) {
                            table[index + 1].push(item)
                        });
                    });

                    new Chartist.Bar('#chart-container', {
                        labels: labels,
                        series: series
                    }, {
                        height: '400px',
                        stackBars: true,
                        axisY: {
                            labelInterpolationFnc: function (value) {
                                return (value / 1000);
                            }
                        }
                    }).on('draw', function (data) {
                        if (data.type === 'bar') {
                            data.element.attr({
                                style: 'stroke-width: 50px'
                            });
                        }
                    });


                    var chart = $("#chart-wrap");
                    chart.slideDown(300);

                    createTable(table);

                    if (chart_data.length >= 4) {
                        $('#repeat-calc, #calc-button').addClass('disabled');
                    } else {
                        $('#repeat-calc, #calc-button').removeClass('disabled');
                    }

                }
            }
        }();

        $('#calc_type').on('change.show_second_step', function () {
            $(this).off('change.show_second_step');
            $('#second-step').fadeIn();
        });

        Calc.init();
    })(jQuery);
});
